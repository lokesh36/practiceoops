package oops;

class Vehicle
{
	String colour;
	String model;
	public void speed() {
		System.out.println("50 kmh");
	}
}

public class Car extends Vehicle {

	public static void main(String[] args) {
		Car c = new Car();
		c.speed();

	}

}
